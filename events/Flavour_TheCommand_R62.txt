
namespace = the_command

country_event = { #Startup Event
	id = the_command.0
	title = the_command.0.t
	desc = the_command.0.d
	picture = LAND_MILITARY_eventPicture

	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		tag = R62
	}

	option = {
		name = the_command.0.a
		ai_chance = {
			factor = 1
		}
		add_army_professionalism = 0.2
	}
}

country_event = { #Shamakhad Resistance - Spawn rebels
	id = the_command.1
	title = the_command.1.t
	desc = the_command.1.d
	picture = ANGRY_MOB_eventPicture

	fire_only_once = yes
	
	trigger = {
		NOT = { is_year = 1460 }
		tag = R62
	}

	mean_time_to_happen = {
		months = 1
	}

	option = { #Destroy them!
		name = the_command.1.a
		ai_chance = {
			factor = 1
		}
		4633 = {
			spawn_rebels = {
				type = nationalist_rebels
				size = 2
				win = yes
			}
		}
		4632 = {
			spawn_rebels = {
				type = nationalist_rebels
				size = 1
				win = yes
			}
		}
		4634 = {
			spawn_rebels = {
				type = nationalist_rebels
				size = 1
				win = yes
			}
		}
		4636 = {
			spawn_rebels = {
				type = nationalist_rebels
				size = 1
				win = yes
			}
		}
		4637 = {
			spawn_rebels = {
				type = nationalist_rebels
				size = 1
				win = yes
			}
		}
	}
}

province_event = { # Orcish slaves in $Provincename$
	id = the_command.2
	title = the_command.2.t
	desc = the_command.2.d
	picture = WESTERNISATION_eventPicture
	goto = root
	
	trigger = {
		owned_by = R62
		has_orcish_minority_trigger = no
		has_orcish_majority_trigger = no
		NOT = {
			has_terrain = cavern
			has_terrain = dwarven_road
			has_terrain = dwarven_hold
			has_terrain = dwarven_hold_surface
			has_terrain = city_terrain
			trade_goods = cloth
			trade_goods = paper
			trade_goods = glass
			trade_goods = chinaware
			trade_goods = slaves
			trade_goods = silk
			trade_goods = dyes
		}
	}

	mean_time_to_happen = {
		years = 50
		
		modifier = {
			factor = 0.6
			OR = {
				trade_goods = tea
				trade_goods = cotton
				trade_goods = spices
				trade_goods = tropical_wood
				trade_goods = tobacco
				trade_goods = sugar
				trade_goods = coffee
				trade_goods = iron
				trade_goods = copper
				trade_goods = gold
			}
		}
		modifier = {
			factor = 0.5
			culture_group = hobgoblin
		}
		modifier = {
			factor = 0.25
			OR = {
				trade_goods = grain
				trade_goods = livestock
			}
		}
		modifier = {
			factor = 2
			unrest = 2
		}
		modifier = {
			factor = 5
			unrest = 5
		}
		modifier = {
			factor = 5
			unrest = 10
		}
	}
	is_mtth_scaled_to_size = 1

	option = { # 
		name = the_command.2.a
		ai_chance = {
			factor = 1
		}
		add_permanent_province_modifier = {
			name = orcish_slave_boom
			duration = 3650
		}
		add_orcish_minority_size_effect = yes
		small_decrease_of_orcish_tolerance_effect = yes
	}
}

country_event = { # The rule of [ROOT.Monarch.GetName]
	id = the_command.3
	title = the_command.3.t
	desc = the_command.3.desc
	picture = SEJM_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_reform = hobgoblin_stratocracy_reform
	}
	
	immediate = {
		hidden_effect = {
			add_legitimacy = 100
			add_legitimacy = -70
		}
	}

	option = { # The time of the Wolf has come
		name = the_command.3.a
		ai_chance = {
			factor = 1
		}
		trigger = {
			OR = {
				NOT = { dynasty = "Boarborn" }
				NOT = { has_faction = hob_boar_command }
			}
			OR = {
				NOT = { dynasty = "Lionborn" }
				NOT = { has_faction = hob_lion_command }
			}
			OR = {
				NOT = { dynasty = "Dragonborn" }
				NOT = { has_faction = hob_dragon_command }
			}
			OR = {
				NOT = { dynasty = "Elephantborn" }
				NOT = { has_faction = hob_elephant_command }
			}
			OR = {
				NOT = { dynasty = "Tigerborn" }
				NOT = { has_faction = hob_tiger_command }
			}
			has_faction = hob_wolf_command
		}
		
		add_faction_influence = {
			faction = hob_wolf_command
			influence = 10
		}
		add_ruler_modifier = {
			name = command_rule_of_the_boar
			duration = -1
		}
		change_primary_culture = boar_command
		set_ruler_culture = boar_command
		
		add_ruler_modifier = {
			name = command_unproven_leadership
			duration = 1825
		}
	}

	option = { # The time of the Boar has come
		name = the_command.3.b
		ai_chance = {
			factor = 1
		}
		trigger = {
			OR = {
				NOT = { dynasty = "Wolfborn" }
				NOT = { has_faction = hob_wolf_command }
			}
			OR = {
				NOT = { dynasty = "Lionborn" }
				NOT = { has_faction = hob_lion_command }
			}
			OR = {
				NOT = { dynasty = "Dragonborn" }
				NOT = { has_faction = hob_dragon_command }
			}
			OR = {
				NOT = { dynasty = "Elephantborn" }
				NOT = { has_faction = hob_elephant_command }
			}
			OR = {
				NOT = { dynasty = "Tigerborn" }
				NOT = { has_faction = hob_tiger_command }
			}
			has_faction = hob_boar_command
		}
		
		add_faction_influence = {
			faction = hob_boar_command
			influence = 10
		}
		add_ruler_modifier = {
			name = command_rule_of_the_boar
			duration = -1
		}
		change_primary_culture = boar_command
		set_ruler_culture = boar_command
		
		add_ruler_modifier = {
			name = command_unproven_leadership
			duration = 1825
		}
	}

	option = { # The time of the Lion has come
		name = the_command.3.c
		ai_chance = {
			factor = 1
		}
		trigger = {
			OR = {
				NOT = { dynasty = "Wolfborn" }
				NOT = { has_faction = hob_wolf_command }
			}
			OR = {
				NOT = { dynasty = "Boarborn" }
				NOT = { has_faction = hob_boar_command }
			}
			OR = {
				NOT = { dynasty = "Dragonborn" }
				NOT = { has_faction = hob_dragon_command }
			}
			OR = {
				NOT = { dynasty = "Elephantborn" }
				NOT = { has_faction = hob_elephant_command }
			}
			OR = {
				NOT = { dynasty = "Tigerborn" }
				NOT = { has_faction = hob_tiger_command }
			}
			has_faction = hob_lion_command
		}
		
		add_faction_influence = {
			faction = hob_lion_command
			influence = 20
		}
		add_ruler_modifier = {
			name = command_rule_of_the_lion
			duration = -1
		}
		change_primary_culture = lion_command
		set_ruler_culture = lion_command
		
		add_ruler_modifier = {
			name = command_unproven_leadership
			duration = 1825
		}
	}

	option = { # The time of the Dragon has come
		name = the_command.3.d
		ai_chance = {
			factor = 1
		}
		trigger = {
			OR = {
				NOT = { dynasty = "Wolfborn" }
				NOT = { has_faction = hob_wolf_command }
			}
			OR = {
				NOT = { dynasty = "Boarborn" }
				NOT = { has_faction = hob_boar_command }
			}
			OR = {
				NOT = { dynasty = "Lionborn" }
				NOT = { has_faction = hob_lion_command }
			}
			OR = {
				NOT = { dynasty = "Elephantborn" }
				NOT = { has_faction = hob_elephant_command }
			}
			OR = {
				NOT = { dynasty = "Tigerborn" }
				NOT = { has_faction = hob_tiger_command }
			}
			has_faction = hob_dragon_command
		}
		
		add_faction_influence = {
			faction = hob_dragon_command
			influence = 10
		}
		add_ruler_modifier = {
			name = command_rule_of_the_dragon
			duration = -1
		}
		change_primary_culture = dragon_command
		set_ruler_culture = dragon_command
		
		add_ruler_modifier = {
			name = command_unproven_leadership
			duration = 1825
		}
	}

	option = { # The time of the Elephant has come
		name = the_command.3.e
		ai_chance = {
			factor = 1
		}
		trigger = {
			OR = {
				NOT = { dynasty = "Wolfborn" }
				NOT = { has_faction = hob_wolf_command }
			}
			OR = {
				NOT = { dynasty = "Boarborn" }
				NOT = { has_faction = hob_boar_command }
			}
			OR = {
				NOT = { dynasty = "Lionborn" }
				NOT = { has_faction = hob_lion_command }
			}
			OR = {
				NOT = { dynasty = "Dragonborn" }
				NOT = { has_faction = hob_dragon_command }
			}
			OR = {
				NOT = { dynasty = "Tigerborn" }
				NOT = { has_faction = hob_tiger_command }
			}
			has_faction = hob_elephant_command
		}
		
		add_faction_influence = {
			faction = hob_elephant_command
			influence = 10
		}
		add_ruler_modifier = {
			name = command_rule_of_the_elephant
			duration = -1
		}
		change_primary_culture = elephant_command
		set_ruler_culture = elephant_command
		
		add_ruler_modifier = {
			name = command_unproven_leadership
			duration = 1825
		}
	}

	option = { # The time of the Tiger has come
		name = the_command.3.f
		ai_chance = {
			factor = 1
		}
		trigger = {
			OR = {
				NOT = { dynasty = "Wolfborn" }
				NOT = { has_faction = hob_wolf_command }
			}
			OR = {
				NOT = { dynasty = "Boarborn" }
				NOT = { has_faction = hob_boar_command }
			}
			OR = {
				NOT = { dynasty = "Lionborn" }
				NOT = { has_faction = hob_lion_command }
			}
			OR = {
				NOT = { dynasty = "Dragonborn" }
				NOT = { has_faction = hob_dragon_command }
			}
			OR = {
				NOT = { dynasty = "Elephantborn" }
				NOT = { has_faction = hob_elephant_command }
			}
			has_faction = hob_tiger_command
		}
		
		add_faction_influence = {
			faction = hob_tiger_command
			influence = 10
		}
		add_ruler_modifier = {
			name = command_rule_of_the_tiger
			duration = -1
		}
		change_primary_culture = tiger_command
		set_ruler_culture = tiger_command
		
		add_ruler_modifier = {
			name = command_unproven_leadership
			duration = 1825
		}
	}
}


#############################
#	Pushing Orc Slaves		#
#############################

country_event = { # Thunderfist Movement Menu
	id = the_command.100
	title = the_command.100.t
	desc = the_command.100.d
	picture = WESTERNISATION_eventPicture

	is_triggered_only = yes
	
	immediate = {
		
		hidden_effect = {
			random_owned_area = {
				limit = {
					superregion = rahen_superregion
					area_for_scope_province = {
						type = all
						owned_by = ROOT
						is_capital = no
					}
					any_neighbor_province = {
						owner = {
							NOT = {
								tag = R62
								is_subject_of = R62
							}
						}
					}
				}
				save_event_target_as = thunderfist_candidate_1
				set_province_flag = thunderfist_candidate_1
			}
			random_owned_area = {
				limit = {
					superregion = rahen_superregion
					area_for_scope_province = {
						type = all
						owned_by = ROOT
						is_capital = no
					}
					NOT = {
						has_province_flag = thunderfist_candidate_1
					}
					any_neighbor_province = {
						owner = {
							NOT = {
								tag = R62
								is_subject_of = R62
							}
						}
					}
				}
				save_event_target_as = thunderfist_candidate_2
				set_province_flag = thunderfist_candidate_2
			}
			random_owned_area = {
				limit = {
					superregion = rahen_superregion
					area_for_scope_province = {
						type = all
						owned_by = ROOT
						is_capital = no
					}
					NOT = {
						has_province_flag = thunderfist_candidate_1
						has_province_flag = thunderfist_candidate_2
					}
					any_neighbor_province = {
						owner = {
							NOT = {
								tag = R62
								is_subject_of = R62
							}
						}
					}
				}
				save_event_target_as = thunderfist_candidate_3
				set_province_flag = thunderfist_candidate_3
			}
		
		}
	}

	option = { # Choose first
		name = the_command.100.a
		trigger = {
			any_owned_province = {
				has_province_flag = thunderfist_candidate_1
			}
		}
		ai_chance = {
			factor = 1
		}
		goto = thunderfist_candidate_1
		
		add_country_modifier = {
			name = hobgoblin_thunderfist_shamans
			duration = 3650
		}
		event_target:thunderfist_candidate_1 = {
			area = {
				cede_province = R63
				if = {
					limit = { is_core = R62 }
					remove_core = R62
					add_core = R63
				}
			}
		}
		R63 = {
			every_owned_province = {
				limit = {
					NOT = { has_province_flag = thunderfist_candidate_1 }
				}
				cede_province = R62
				if = {
					limit = { is_core = R63 }
					remove_core = R63
					add_core = R62
				}
			}
			random_owned_province = {
				limit = {
					NOT = { has_province_flag = hobgoblin_has_orcish_minority }
				}
				add_core = PREV
				change_culture = PREV
				change_religion = PREV
				add_base_manpower = 1
			}
			random_owned_area = { set_province_flag = hobgoblin_has_orcish_minority }
			hidden_effect = {
				every_owned_province = {
					limit = {
						has_province_flag = thunderfist_candidate_1
					}
					clr_province_flag = thunderfist_candidate_1
				}
			}
		}
		hidden_effect = {
			every_owned_province = {
				clr_province_flag = thunderfist_candidate_2
				clr_province_flag = thunderfist_candidate_3
			}
		}
		
		close_single_menu = yes
	}

	option = { # Choose second
		name = the_command.100.b
		trigger = {
			any_owned_province = {
				has_province_flag = thunderfist_candidate_2
			}
		}
		ai_chance = {
			factor = 1
		}
		goto = thunderfist_candidate_2
		
		add_country_modifier = {
			name = hobgoblin_thunderfist_shamans
			duration = 3650
		}
		event_target:thunderfist_candidate_2 = {
			area = {
				cede_province = R63
				if = {
					limit = { is_core = R62 }
					remove_core = R62
					add_core = R63
				}
			}
		}
		R63 = {
			every_owned_province = {
				limit = {
					NOT = { has_province_flag = thunderfist_candidate_2 }
				}
				cede_province = R62
				if = {
					limit = { is_core = R63 }
					remove_core = R63
					add_core = R62
				}
			}
			random_owned_province = {
				limit = {
					NOT = { has_province_flag = hobgoblin_has_orcish_minority }
				}
				add_core = PREV
				change_culture = PREV
				change_religion = PREV
				add_base_manpower = 1
			}
			random_owned_area = { set_province_flag = hobgoblin_has_orcish_minority }
			hidden_effect = {
				every_owned_province = {
					limit = {
						has_province_flag = thunderfist_candidate_2
					}
					clr_province_flag = thunderfist_candidate_2
				}
			}
		}
		hidden_effect = {
			every_owned_province = {
				clr_province_flag = thunderfist_candidate_1
				clr_province_flag = thunderfist_candidate_3
			}
		}
		
		close_single_menu = yes
	}

	option = { # Choose third
		name = the_command.100.c
		trigger = {
			any_owned_province = {
				has_province_flag = thunderfist_candidate_3
			}
		}
		ai_chance = {
			factor = 1
		}
		goto = thunderfist_candidate_3
		
		add_country_modifier = {
			name = hobgoblin_thunderfist_shamans
			duration = 3650
		}
		event_target:thunderfist_candidate_3 = {
			area = {
				cede_province = R63
				if = {
					limit = { is_core = R62 }
					remove_core = R62
					add_core = R63
				}
			}
		}
		R63 = {
			every_owned_province = {
				limit = {
					NOT = { has_province_flag = thunderfist_candidate_3 }
				}
				cede_province = R62
				if = {
					limit = { is_core = R63 }
					remove_core = R63
					add_core = R62
				}
			}
			random_owned_province = {
				limit = {
					NOT = { has_province_flag = hobgoblin_has_orcish_minority }
				}
				change_culture = PREV
				change_religion = PREV
				add_core = PREV
				add_base_manpower = 1
			}
			random_owned_area = { set_province_flag = hobgoblin_has_orcish_minority }
			hidden_effect = {
				every_owned_province = {
					limit = {
						has_province_flag = thunderfist_candidate_3
					}
					clr_province_flag = thunderfist_candidate_3
				}
			}
		}
		hidden_effect = {
			every_owned_province = {
				clr_province_flag = thunderfist_candidate_1
				clr_province_flag = thunderfist_candidate_2
			}
		}
		
		close_single_menu = yes
	}
	
	option = {
		name = the_command.100.dd
		highlight = yes
		
		close_single_menu = yes
		hidden_effect = {
			every_owned_province = {
				clr_province_flag = thunderfist_candidate_1
				clr_province_flag = thunderfist_candidate_2
				clr_province_flag = thunderfist_candidate_3
			}
		}
	}
}

country_event = { # Bloodsong Movement Menu
	id = the_command.101
	title = the_command.101.t
	desc = the_command.101.d
	picture = WESTERNISATION_eventPicture

	is_triggered_only = yes
	
	immediate = {
		
		hidden_effect = {
			random_owned_area = {
				limit = {
					superregion = rahen_superregion
					area_for_scope_province = {
						type = all
						owned_by = ROOT
						is_capital = no
					}
					any_neighbor_province = {
						owner = {
							NOT = {
								tag = R62
								is_subject_of = R62
							}
						}
					}
				}
				save_event_target_as = bloodsong_candidate_1
				set_province_flag = bloodsong_candidate_1
			}
			random_owned_area = {
				limit = {
					superregion = rahen_superregion
					area_for_scope_province = {
						type = all
						owned_by = ROOT
						is_capital = no
					}
					NOT = {
						has_province_flag = bloodsong_candidate_1
					}
					any_neighbor_province = {
						owner = {
							NOT = {
								tag = R62
								is_subject_of = R62
							}
						}
					}
				}
				save_event_target_as = bloodsong_candidate_2
				set_province_flag = bloodsong_candidate_2
			}
			random_owned_area = {
				limit = {
					superregion = rahen_superregion
					area_for_scope_province = {
						type = all
						owned_by = ROOT
						is_capital = no
					}
					NOT = {
						has_province_flag = bloodsong_candidate_1
						has_province_flag = bloodsong_candidate_2
					}
					any_neighbor_province = {
						owner = {
							NOT = {
								tag = R62
								is_subject_of = R62
							}
						}
					}
				}
				save_event_target_as = bloodsong_candidate_3
				set_province_flag = bloodsong_candidate_3
			}
		
		}
	}

	option = { # Choose first
		name = the_command.101.a
		trigger = {
			any_owned_province = {
				has_province_flag = bloodsong_candidate_1
			}
		}
		ai_chance = {
			factor = 1
		}
		goto = bloodsong_candidate_1
		
		add_country_modifier = {
			name = hobgoblin_bloodsong_advance_guard
			duration = 3650
		}
		event_target:bloodsong_candidate_1 = {
			area = {
				cede_province = R64
				if = {
					limit = { is_core = R62 }
					remove_core = R62
					add_core = R64
				}
			}
		}
		R64 = {
			every_owned_province = {
				limit = {
					NOT = { has_province_flag = bloodsong_candidate_1 }
				}
				cede_province = R62
				if = {
					limit = { is_core = R64 }
					remove_core = R64
					add_core = R62
				}
			}
			random_owned_province = {
				limit = {
					NOT = { has_province_flag = hobgoblin_has_orcish_minority }
				}
				add_core = PREV
				change_culture = PREV
				change_religion = PREV
				add_base_manpower = 1
			}
			random_owned_area = { set_province_flag = hobgoblin_has_orcish_minority }
			hidden_effect = {
				every_owned_province = {
					limit = {
						has_province_flag = bloodsong_candidate_1
					}
					clr_province_flag = bloodsong_candidate_1
				}
			}
		}
		hidden_effect = {
			every_owned_province = {
				clr_province_flag = bloodsong_candidate_2
				clr_province_flag = bloodsong_candidate_3
			}
		}
		
		close_single_menu = yes
	}

	option = { # Choose second
		name = the_command.101.b
		trigger = {
			any_owned_province = {
				has_province_flag = bloodsong_candidate_2
			}
		}
		ai_chance = {
			factor = 1
		}
		goto = bloodsong_candidate_2
		
		add_country_modifier = {
			name = hobgoblin_bloodsong_advance_guard
			duration = 3650
		}
		event_target:bloodsong_candidate_2 = {
			area = {
				cede_province = R64
				if = {
					limit = { is_core = R62 }
					remove_core = R62
					add_core = R64
				}
			}
		}
		R64 = {
			every_owned_province = {
				limit = {
					NOT = { has_province_flag = bloodsong_candidate_2 }
				}
				cede_province = R62
				if = {
					limit = { is_core = R64 }
					remove_core = R64
					add_core = R62
				}
			}
			random_owned_province = {
				limit = {
					NOT = { has_province_flag = hobgoblin_has_orcish_minority }
				}
				add_core = PREV
				change_culture = PREV
				change_religion = PREV
				add_base_manpower = 1
			}
			random_owned_area = { set_province_flag = hobgoblin_has_orcish_minority }
			hidden_effect = {
				every_owned_province = {
					limit = {
						has_province_flag = bloodsong_candidate_2
					}
					clr_province_flag = bloodsong_candidate_2
				}
			}
		}
		hidden_effect = {
			every_owned_province = {
				clr_province_flag = bloodsong_candidate_1
				clr_province_flag = bloodsong_candidate_3
			}
		}
		
		close_single_menu = yes
	}

	option = { # Choose third
		name = the_command.101.c
		trigger = {
			any_owned_province = {
				has_province_flag = bloodsong_candidate_3
			}
		}
		ai_chance = {
			factor = 1
		}
		goto = bloodsong_candidate_3
		
		add_country_modifier = {
			name = hobgoblin_bloodsong_advance_guard
			duration = 3650
		}
		event_target:bloodsong_candidate_3 = {
			area = {
				cede_province = R64
				if = {
					limit = { is_core = R62 }
					remove_core = R62
					add_core = R64
				}
			}
		}
		R64 = {
			every_owned_province = {
				limit = {
					NOT = { has_province_flag = bloodsong_candidate_3 }
				}
				cede_province = R62
				if = {
					limit = { is_core = R64 }
					remove_core = R64
					add_core = R62
				}
			}
			random_owned_province = {
				limit = {
					NOT = { has_province_flag = hobgoblin_has_orcish_minority }
				}
				add_core = PREV
				change_culture = PREV
				change_religion = PREV
				add_base_manpower = 1
			}
			random_owned_area = { set_province_flag = hobgoblin_has_orcish_minority }
			hidden_effect = {
				every_owned_province = {
					limit = {
						has_province_flag = bloodsong_candidate_3
					}
					clr_province_flag = bloodsong_candidate_3
				}
			}
		}
		hidden_effect = {
			every_owned_province = {
				clr_province_flag = bloodsong_candidate_1
				clr_province_flag = bloodsong_candidate_2
			}
		}
		
		close_single_menu = yes
	}
	
	option = {
		name = the_command.101.dd
		highlight = yes
		
		close_single_menu = yes
		hidden_effect = {
			every_owned_province = {
				clr_province_flag = bloodsong_candidate_1
				clr_province_flag = bloodsong_candidate_2
				clr_province_flag = bloodsong_candidate_3
			}
		}
	}
}

################
#PUNISHEDHOBGOB#
################
country_event = {
	id = the_command.201
	title = the_command.201.t
	desc = the_command.201.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	fire_only_once = yes

	trigger = {
		tag = R62
		owns_or_subject_of = 4941
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = { #Good riddance.
		name = the_command.201.a
		ai_chance = {
			factor = 1
		}
		hidden_effect = {
			set_global_flag = ronin_executed
			4941 = {
				remove_province_modifier = hobgoblin_minority_oppressed_small
				remove_province_modifier = hobgoblin_minority_oppressed_large
				remove_province_modifier = hobgoblin_minority_coexisting_small
				remove_province_modifier = hobgoblin_minority_coexisting_large
				remove_province_modifier = hobgoblin_minority_integrated_small
				remove_province_modifier = hobgoblin_minority_integrated_large
			}
		}
		custom_tooltip = executing_ronin_tooltip
		4941 = {
			add_devastation = 35
		}
	}
}

########################
###   The War Room   ###
########################

country_event = {
	id = the_command.999
	title = the_command.999.t
	desc = the_command.999.desc
	picture = NOBLE_ESTATE_DEMANDS_eventPicture

	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		has_reform = hobgoblin_stratocracy_reform
		has_country_flag = command_war_room
	}
	
	option = { #estates present an agenda
		name = the_command.999.a
		trigger = { NOT = { has_any_active_estate_agenda = yes } }
		
		country_event = { id = the_command.1000 }
	}
	option = { #estates present something else
		name = the_command.999.b
		trigger = { has_any_active_estate_agenda = yes }
		
		country_event = { id = the_command.1001 }
	}
}

country_event = {
	id = the_command.1000
	title = the_command.1000.t
	desc = the_command.1000.desc
	picture = NOBLE_ESTATE_DEMANDS_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_reform = hobgoblin_stratocracy_reform
	}
	
	immediate = {
		hidden_effect = {
			pick_random_estate_if_present = { flag = present_agenda estate_action = generate_estate_agenda }
			pick_random_estate_if_present = { flag = present_agenda estate_action = generate_estate_agenda }
			pick_random_estate_if_present = { flag = present_agenda estate_action = generate_estate_agenda }
		}
	}

	after = {
		clr_country_flag = estate_wolf_command_present_agenda
		clr_country_flag = estate_boar_command_present_agenda
		clr_country_flag = estate_lion_command_present_agenda
		clr_country_flag = estate_dragon_command_present_agenda
		clr_country_flag = estate_elephant_command_present_agenda
		clr_country_flag = estate_tiger_command_present_agenda
		clr_country_flag = estate_gerunanin_present_agenda
		clear_estate_agenda_cache = ROOT
	}

	option = {
		name = the_command.1000.a
		trigger = { has_country_flag = estate_wolf_command_present_agenda }
		start_estate_agenda = estate_wolf_command
		ai_chance = {
			factor = 1
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						loyalty = 30
						estate = estate_wolf_command
					}
				}
			}
			modifier = {
				factor = 2
				NOT = {
					estate_loyalty = {
						loyalty = 20
						estate = estate_wolf_command
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					influence = 70
					estate = estate_wolf_command
				}
			}
			modifier = {
				factor = 2
				is_absolutism_active = yes
				num_of_estate_privileges = {
					estate = estate_wolf_command
					value = 1
				}
				estate_loyalty = {
					estate = estate_wolf_command
					higher_than_influence = no
				}
			}
		}
	}
	
	option = {
		name = the_command.1000.b
		trigger = { has_country_flag = estate_boar_command_present_agenda }
		start_estate_agenda = estate_boar_command
		ai_chance = {
			factor = 1
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						loyalty = 30
						estate = estate_boar_command
					}
				}
			}
			modifier = {
				factor = 2
				NOT = {
					estate_loyalty = {
						loyalty = 20
						estate = estate_boar_command
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					influence = 70
					estate = estate_boar_command
				}
			}
			modifier = {
				factor = 2
				is_absolutism_active = yes
				num_of_estate_privileges = {
					estate = estate_boar_command
					value = 1
				}
				estate_loyalty = {
					estate = estate_boar_command
					higher_than_influence = no
				}
			}
		}
	}
	
	option = {
		name = the_command.1000.c
		trigger = { has_country_flag = estate_lion_command_present_agenda }
		start_estate_agenda = estate_lion_command
		ai_chance = {
			factor = 1
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						loyalty = 30
						estate = estate_lion_command
					}
				}
			}
			modifier = {
				factor = 2
				NOT = {
					estate_loyalty = {
						loyalty = 20
						estate = estate_lion_command
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					influence = 70
					estate = estate_lion_command
				}
			}
			modifier = {
				factor = 2
				is_absolutism_active = yes
				num_of_estate_privileges = {
					estate = estate_lion_command
					value = 1
				}
				estate_loyalty = {
					estate = estate_lion_command
					higher_than_influence = no
				}
			}
		}
	}
	
	option = {
		name = the_command.1000.d
		trigger = { has_country_flag = estate_dragon_command_present_agenda }
		start_estate_agenda = estate_dragon_command
		ai_chance = {
			factor = 1
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						loyalty = 30
						estate = estate_dragon_command
					}
				}
			}
			modifier = {
				factor = 2
				NOT = {
					estate_loyalty = {
						loyalty = 20
						estate = estate_dragon_command
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					influence = 70
					estate = estate_dragon_command
				}
			}
			modifier = {
				factor = 2
				is_absolutism_active = yes
				num_of_estate_privileges = {
					estate = estate_dragon_command
					value = 1
				}
				estate_loyalty = {
					estate = estate_dragon_command
					higher_than_influence = no
				}
			}
		}
	}
	
	option = {
		name = the_command.1000.e
		trigger = { has_country_flag = estate_elephant_command_present_agenda }
		start_estate_agenda = estate_elephant_command
		ai_chance = {
			factor = 1
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						loyalty = 30
						estate = estate_elephant_command
					}
				}
			}
			modifier = {
				factor = 2
				NOT = {
					estate_loyalty = {
						loyalty = 20
						estate = estate_elephant_command
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					influence = 70
					estate = estate_elephant_command
				}
			}
			modifier = {
				factor = 2
				is_absolutism_active = yes
				num_of_estate_privileges = {
					estate = estate_elephant_command
					value = 1
				}
				estate_loyalty = {
					estate = estate_elephant_command
					higher_than_influence = no
				}
			}
		}
	}
	
	option = {
		name = the_command.1000.f
		trigger = { has_country_flag = estate_tiger_command_present_agenda }
		start_estate_agenda = estate_tiger_command
		ai_chance = {
			factor = 1
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						loyalty = 30
						estate = estate_tiger_command
					}
				}
			}
			modifier = {
				factor = 2
				NOT = {
					estate_loyalty = {
						loyalty = 20
						estate = estate_tiger_command
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					influence = 70
					estate = estate_tiger_command
				}
			}
			modifier = {
				factor = 2
				is_absolutism_active = yes
				num_of_estate_privileges = {
					estate = estate_tiger_command
					value = 1
				}
				estate_loyalty = {
					estate = estate_tiger_command
					higher_than_influence = no
				}
			}
		}
	}
	
	option = {
		name = the_command.1000.g
		trigger = { has_country_flag = estate_gerunanin_present_agenda }
		start_estate_agenda = estate_gerunanin
		ai_chance = {
			factor = 1
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						loyalty = 30
						estate = estate_gerunanin
					}
				}
			}
			modifier = {
				factor = 2
				NOT = {
					estate_loyalty = {
						loyalty = 20
						estate = estate_gerunanin
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					influence = 70
					estate = estate_gerunanin
				}
			}
			modifier = {
				factor = 2
				is_absolutism_active = yes
				num_of_estate_privileges = {
					estate = estate_gerunanin
					value = 1
				}
				estate_loyalty = {
					estate = estate_gerunanin
					higher_than_influence = no
				}
			}
		}
	}
}

country_event = {
	id = the_command.1001
	title = the_command.1001.t
	desc = the_command.1001.desc
	picture = NOBLE_ESTATE_DEMANDS_eventPicture

	is_triggered_only = yes
	
	trigger = {
		has_reform = hobgoblin_stratocracy_reform
	}
	
	option = { 
		name = the_command.1001.a
		
		
	}
	option = { 
		name = the_command.1001.b
		
		
	}
}
